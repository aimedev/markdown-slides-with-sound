# A simple slideshow!

A simple, in-browser, slideshow system where you only have to edit one file. Use the powerful Markdown language in the `SLIDESHOW.md` file and add text content, images, and sounds!

## Requirements

- A FTP access to your own webhosted space (from any provider)
- Some creativity ✨

sudo is NOT required

## Using this project

You have to clone this repository in the directory in which you will deploy your slideshow as a website.

```bash
cd /path/to/your/website
git clone https://gitlab.com/aimedev/slideshow slideshow
```

## How to

You will have to edit the `SLIDESHOW.md` file to add your slide content.
Please refer to the `_docs/howto/howto-basic.md` file for a full explanation of the elements.

## Slideshow parameters

You will find these values in the `CONFIG.txt` file:
- width (in pixels)
- height (in pixels)
- title (the webpage title)
- favicon (the webpage icon)
- nocache (`yes` for debugging, `no` in production)
- responsive (for mobile users)
- use_sounds (`no` to disable the howl statement)
- default_transition (for each slide which doesn't contain a `=> transition` statement)

## Authors

- aimedev (https://gitlab.com/aimedev)
