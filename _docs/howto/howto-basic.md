# How to: basic stuff

Here is a list of available elements to use in the file `SLIDESHOW.md` which is the main file you have to edit.  
*Please understand that you don't have to use all of the elements described here to create a great slideshow.*

## Define a new slide
Please use the following line to define a new slide with a unique ID (which is a number). Each line from this declaration will be part of the content of the declared slide.  
To declare the next slide, just use the following line again with a different number.
```markdown
=> slide NUMBER
```

## Write the slide content
Just use the [Markdown language](https://www.markdownguide.org/basic-syntax/) to format your slide content! Slide examples are provided at the end of this README.

## Move to another slide
The navigation is easy as you just have to define a button with the wanted slide ID:
```markdown
[Next ➜](3)
```
*In this example, a button with the following label "Next ➜" is created. It displays the slide 3 to the user.*

## Add transitions between slides
```
=> transition NAME
```
Here is a list of the available transitions (use one to replace NAME):
- from-bottom
- from-right
