# Use images in your slideshow!

## Add an image
```markdown
=> image path/to/your/image.png
=> image path/to/your/image.gif (shadow)
=> image https://www.example.web/example-image.jpg
```
