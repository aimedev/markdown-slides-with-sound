# For power users only!

## Add custom Javascript

You have to edit the `scripts/_custom.js` file.

## Add custom styles

You have to edit the `styles/_custom.css` file.  
OR  
You can use the style statement to insert inline CSS instructions.
```markdown
=> style --slideshow: gray
=> style color: red
=> style padding: 10px; margin: 15px
```