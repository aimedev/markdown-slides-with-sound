# Use sounds in your slideshow!

You can use any sound from Internet with a common music extension (.mp3, .wav, etc), but you should upload your own files in the `sound` folder at the root of the project.

## Start playing a sound
```markdown
=> howl path/to/your/sound.mp3
=> howl https://www.example.web/files/a-very-cool-sound.mp3
```

## Stop the player
```markdown
=> howl stop
```
