# Templates

## Use a template for a slide
```markdown
=> template NAME
```
The available templates are the following:
* `basic`
* `title`
* `gallery NUMBER-OF-IMAGES` (e.g : `gallery 5`)

## Define a image included in a template
```markdown
=> image images/example.jpeg
=> image https://www.example.web/path/to/your/image.png
```
