=> slide 1
=> template title
=> transition from-bottom
=> howl stop

=> image images/logo.jpg (logo)

# Hello world!

Every element is **customizable**.  
Try editing this slideshow!

[Next slide ➜](2)


=> slide 2
=> template basic
=> transition from-bottom
=> style --slideshow: #eeeeee

# This is a simple title

One line for beginning the description  
Another line, only because I decided to add one

=> image /images/humanum.jpg (img shadow)

[Next ➜](3)


=> slide 3
=> template gallery 3

# This is a gallery-like slide

=> image /images/humanum.jpg (img)  
=> image /images/humanum.jpg (img shadow)  
=> image /images/humanum.jpg (img)  

[Next ➜](4)


=> slide 4
=> style ".content > *" margin-bottom: 20px;
=> style ".content > .image" margin: 40px 0;

# This slide has some sound

It is loaded from **/sound/scatman.mp3**. Can you hear it?  

=> howl sound/scatman.mp3

=> image images/scatman.jpg

[Restart ?](1)