class HowlManager {

	constructor() {
		if (window.slideshow.config.use_sounds !== 'yes')
			return;
		
		if (typeof Howl != 'null') {
			this.enabled = true;
			this.player = null;

		} else {
			this.enabled = false;
		}
	}

	play() {
		this.stop();
		this.player = new Howl({
			src: [window.slideshow.currentSlide.howl],
			autoplay: true,
			onend: function() {
				window.slideshow.howl.updateButton();
			}
		});
		this.player.play();
	}

	stop() {
		if (this.player !== null) {
			this.player.stop();
		}
	}

	pause() {
		if (this.player !== null) {
			this.player.pause();
		}
	}

	playing() {
		return (this.player && this.player.playing());
	}

	updateButton() {
		const buttonElement = document.querySelector('.howl');
		if (!window.slideshow.howl.playing()) {
			buttonElement.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
				<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5.586 15H4a1 1 0 01-1-1v-4a1 1 0 011-1h1.586l4.707-4.707C10.923 3.663 12 4.109 12 5v14c0 .891-1.077 1.337-1.707.707L5.586 15z" clip-rule="evenodd" />
				<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2" />
			</svg>`;
		} else {
			buttonElement.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
				<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.536 8.464a5 5 0 010 7.072m2.828-9.9a9 9 0 010 12.728M5.586 15H4a1 1 0 01-1-1v-4a1 1 0 011-1h1.586l4.707-4.707C10.923 3.663 12 4.109 12 5v14c0 .891-1.077 1.337-1.707.707L5.586 15z" />
			</svg>`;
		}
	}
}

export { HowlManager };