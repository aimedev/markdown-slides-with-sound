import { Dom } from './Dom.js';
import { HowlManager } from './HowlManager.js';


class Slideshow {

	constructor() {
		this.config = {};
		this.currentSlide = {
			type: 'none',
			lines: [],
			transition: 'none',
			howl: null
		};
		this.sliding = false;
	}

	firstLoad() {
		// DOM events
		this.dom = new Dom();
		this.dom.initEvents();

		// Configuration
		this.getConfiguration()
		.then((response) => {
			const fileLines = response.split(/[\n\r]/);
			for (let line of fileLines) {
				const equalPosition = line.indexOf('=');
				if (!equalPosition)
					continue;
				const configKey = line.substring(0, equalPosition);
				const configValue = line.substring(equalPosition + 1);
				if (!configKey || !configValue)
					continue;
				this.config[configKey] = configValue;
			}

			const slideshow = document.querySelector('.slideshow');
			slideshow.style = `--slide-width: ${this.config['width']}px; --slide-height: ${this.config['height']}px`;

			// Other stuff
			this.dom.addMetaTags();

			// For sounds
			this.howl = new HowlManager();
		});
	}

	getConfiguration() {
		return new Promise((resolve) => {
			var req = new XMLHttpRequest();
			req.open('GET', 'CONFIG.txt', true);
			req.setRequestHeader('Content-Type', 'text/plain');
			req.onload = function(e) {
				resolve(req.response);
			}
			req.send();
		});
	}

	loadSlideshow() {
		this.slides = {};
		this.getSlideshowContentRaw()
		.then((slideShowContent) => {
			const contentSplitted = slideShowContent.split(/[\n\r]/);
			let newSlideID = 0;
			let newSlideContent = '';
			for (let line of contentSplitted) {
				if (!line || /^\s*$/.test(line)) continue; // empty line
				if (line.startsWith('=> slide ')) {
					if (newSlideID > 0) {
						this.slides[newSlideID] = newSlideContent;
					}
					newSlideContent = '';
					newSlideID++;
				} else {
					newSlideContent += `\n${line}`;
				}
			}
			this.slides[newSlideID] = newSlideContent;

			const firstSlideID = Object.keys(this.slides)[0];
			if (firstSlideID > 0) {
				this.loadSlide(firstSlideID);
			} else {
				document.querySelector('.slideshow').innerHTML = `<div class="slide slide-0">
					<h1>Your slideshow is empty!</h1>
					<p>Start writing your content in the file named SLIDESHOW.md</p>
				</div>`;
			}
		});
	}

	loadSlide(slide) {
		if (this.sliding)
			return;
		
		this.sliding = true;
		const slideshow = document.querySelector('.slideshow');
		const slideContent = this.getSlideContentRaw(slide);
		const slideContentSplitted = slideContent.split(/[\n\r]/);
		let slideLines = [];
		let slideStyle = '';
		this.currentSlide.type = 'none';
		this.currentSlide.transition = this.config.default_transition || 'none';
		this.currentSlide.howl = null;
		this.currentSlide.extraStyles = {};
		for (let line of slideContentSplitted) {
			if (!line || /^\s*$/.test(line)) continue;
			const lineObject = {};
			if (line.substring(line.substring(0, line.length - 2), line.length) === '  ') {
				line = line.substring(0, line.length - 2);
			}
			if (line.startsWith('=> template ') && this.currentSlide.type === 'none') {
				line = line.substring(12);
				let slideType = line;
				let slideParameters = [];
				if (line.indexOf(' ') != -1) {
					slideType = slideType.substring(0, line.indexOf(' '));
					slideParameters = line.substring(line.indexOf(' ') + 1).split(' ');
				}
				this.currentSlide.type = slideType;
				this.currentSlide.typeParameters = slideParameters;
				continue;
			}
			if (line.startsWith('=> transition ')) {
				this.currentSlide.transition = line.substring(14);
				continue;
			}
			if (line.startsWith('=> style ')) {
				const styleMatch = line.match(/=> style "(.*)" (.*)/) || [];
				if (styleMatch.length > 0) {
					const elementChosen = styleMatch[1];
					const elementStyle = styleMatch[2];
					if (typeof elementStyle == 'null')
						continue;
					if (elementChosen === 'slide') {
						slideStyle += `${elementStyle};`;
					} else if (typeof elementChosen != 'null') {
						this.currentSlide.extraStyles[elementChosen] = elementStyle;
					}
				}
				continue;
			}
			if (line.startsWith('=> howl ')) {
				this.currentSlide.howl = line.substring(8);
			}
			lineObject.content = line;
			slideLines.push(lineObject);
		}
		this.currentSlide.lines = slideLines;
		const slideHtml = this.getSlideContentHTML(
			this.currentSlide.type,
			this.currentSlide.typeParameters,
			this.currentSlide.lines
		);
		let slideExtraStyles = '';
		for (let s in this.currentSlide.extraStyles) {
			const extraStyle = this.currentSlide.extraStyles[s];
			slideExtraStyles += `${s} { ${extraStyle} }`;
		}
		if (slideExtraStyles !== '')
			slideExtraStyles = `<style>${slideExtraStyles}</style>`;

		slideshow.setAttribute('data-transition', this.currentSlide.transition);
		slideshow.insertAdjacentHTML('beforeend', `<div class="slide slide-${slide}" style="${slideStyle}">
			${slideExtraStyles}
			${slideHtml}
		</div>`);

		const slides = slideshow.querySelectorAll('.slide');
		if (slides.length === 1) { // Première slide
			slides[0].classList.add('new-slide');
			delete this.sliding;

		} else { // Toutes les slides
			slides[0].classList.remove('new-slide');
			slides[0].classList.add('old-slide');
			slides[1].classList.add('new-slide');
			setTimeout(() => {
				slides[0].outerHTML = '';
				delete this.sliding;
			}, 1000);
		}

		// Howl
		if (this.currentSlide.howl !== null) {
			if (this.currentSlide.howl.startsWith('stop')) {
				this.howl.stop();
			} else {
				this.howl.play();
			}
		}
	}

	getSlideshowContentRaw() {
		return new Promise((resolve) => {
			var req = new XMLHttpRequest();
			req.open('GET', `SLIDESHOW.md`, true);
			if (this.config.nocache === 'yes')
				req.setRequestHeader("Cache-Control", "no-cache, no-store, max-age=0");
			req.onload = function(e) {
				resolve(req.response);
			}
			req.send();
		});
	}

	getSlideContentRaw(slide) {
		return this.slides[slide] || "[404] This slide hasn't been defined";
	}

	getSlideContentHTML(type = 'none', typeParameters = [], slideLines = []) {
		let html = `<div class="content`
		if (type !== 'none') {
			html += ` ${type}-template" style="`;
			for (let p in typeParameters) {
				html += ` --param${parseInt(p, 10)+1}: ${typeParameters[p]};`;
			}
		}
		html += '">';
	
		if (type === 'gallery') {
			var ajoutDebutGalerie = false;
			var ajoutFinGalerie = false;
		}
	
		for (let line of slideLines) {
			const lineContent = line.content;

			const howlMatch = lineContent.match(/=> howl ([^ ]*)/) || [];
			if (howlMatch.length > 0) {
				if (lineContent.indexOf('stop') > 0)
					continue;
				html += `<div class="howl">
					<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
						<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.536 8.464a5 5 0 010 7.072m2.828-9.9a9 9 0 010 12.728M5.586 15H4a1 1 0 01-1-1v-4a1 1 0 011-1h1.586l4.707-4.707C10.923 3.663 12 4.109 12 5v14c0 .891-1.077 1.337-1.707.707L5.586 15z" />
					</svg></div>`;
				continue;
			}
			
			const imageMatches = lineContent.match(/=> image ([^ ]*)( \((.*)\))?/) || [];
			if (imageMatches.length > 0) {
				if (type === 'gallery' && !ajoutDebutGalerie) {
					ajoutDebutGalerie = true;
					html += '<div class="gallery">';
				}
				if (imageMatches[1].indexOf('http') === -1)
					imageMatches[1] = '../' + imageMatches[1];
				let imageClasses = `class="${imageMatches[3] || 'image'}"`;
				html += `<div ${imageClasses} style="--image: url('${imageMatches[1]}');"></div>`;
				continue;
	
			} else {
				if (type === 'gallery' && ajoutDebutGalerie && !ajoutFinGalerie) {
					ajoutFinGalerie = true;
					html += '</div>';
				}
			}
	
			const hrefMatches = lineContent.match(/\[(.*)\]\((.*)\)/) || [];
			if (hrefMatches.length > 0) {
				if (parseInt(hrefMatches[2], 10)) {
					html += `<a class="btn" data-slide="${hrefMatches[2]}">${hrefMatches[1]}</a>`;
				} else {
					html += `<a class="external" href="${hrefMatches[2]}" target="_blank">${hrefMatches[1]}</a>`;
				}
				continue;
			}
	
			const parsedLineContent = marked.parse(lineContent);
			if (parsedLineContent !== lineContent) {
				html += parsedLineContent;
			} else {
				html += `<p>${lineContent}</p>`;
			}
		}
		
		html += `</div>`;
		return html;
	}
}

export { Slideshow };
