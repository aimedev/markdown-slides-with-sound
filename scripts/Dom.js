class Dom {

	constructor() {

	}

	jEvent(eventName, childSelector, cb) {
		parent = document.documentElement;
		eventName = eventName.split(' ');
		for (let name of eventName) {
			parent.addEventListener(name, function(event){
				const clickedElement = event.target,
				matchingChild = clickedElement.closest(childSelector);
				if (matchingChild) cb(matchingChild, event)
			});
		}
	}

	initEvents() {
		this.jEvent('click', '[data-slide]', (element, event) => {
			const slide = element.dataset.slide;
			if (!slide)
				return;
			
			window.slideshow.loadSlide(slide);
		});

		this.jEvent('click', '.howl', (element, event) => {
			if (window.slideshow.howl == null)
				return;
		
			if (window.slideshow.howl.playing()) {
				window.slideshow.howl.pause();
			} else {
				window.slideshow.howl.play();
			}
			
			window.slideshow.howl.updateButton();
		});
	}

	addMetaTags() {
		let headAfterBegin = '';
		const slideshowTitle = window.slideshow.config.title;
		if (slideshowTitle) {
			headAfterBegin += `<title>${slideshowTitle}</title>`;
		}
		const faviconURL = window.slideshow.config.favicon;
		if (faviconURL) {
			const faviconCheckExtension = faviconURL.match(/\.([a-z]*)$/);
			if (faviconCheckExtension) {
				const faviconExtension = faviconCheckExtension[1];
				headAfterBegin += `<link rel="icon" type="image/${faviconExtension}" href="${faviconURL}">`;
			}
		}
		const noCache = window.slideshow.config.nocache;
		if (noCache === 'yes') {
			headAfterBegin += `<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
				<meta http-equiv="Pragma" content="no-cache">
				<meta http-equiv="Expires" content="0">`;
		}

		const responsive = window.slideshow.config.responsive;
		if (responsive === 'yes') {
			headAfterBegin += `<meta name="viewport" content="width=device-width, initial-scale=1">`;
		}

		document.querySelector('head').insertAdjacentHTML('afterbegin', headAfterBegin);
	}
}

export { Dom };